const bcrypt = require("bcrypt");
const saltRounds = 10;

const hashedPassword = async (password) => {
  return await bcrypt.hash(password, saltRounds);
};

const CheckPassword = async (password, dbpassword) => {
  return await bcrypt.compare(password, dbpassword);
};

module.exports = { hashedPassword, CheckPassword };
