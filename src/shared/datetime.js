const currentDateTime = () => {
  const currentTime = new Date();
  const currentoffset = currentTime.getTimezoneOffset();
  const ISTOffset = 330;

  var d = new Date(currentTime.getTime() + (ISTOffset + currentoffset) * 60000);

  return (
    d.getFullYear() +
    "-" +
    (d.getMonth() + 1) +
    "-" +
    d.getDate() +
    " " +
    d.getHours() +
    ":" +
    d.getMinutes()
  );
};

module.exports = currentDateTime;
