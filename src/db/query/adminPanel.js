const { Database } = require("../../../index");

const Myconnection = Database.collection("users");

const userView = (sortObj, skip, limit) =>
  Myconnection.find({},{
    projection: {
      _id: 0,
      userid: 1,
      fname: 1,
      email: 1,
      type: 1,
      isActive: 1,
    },
  }).sort(sortObj).skip(skip).limit(limit).toArray();

module.exports = userView;
