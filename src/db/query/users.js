const { Database } = require("../../../index");

const Myconnection = Database.collection("users");

const findUser = (id) => Myconnection.findOne({ id: id });

const findUserByEmail = (email) => Myconnection.findOne({ email: email });

const sharedUser = (userid) =>
  Myconnection.findOne(
    { id: userid },
    {
      projection: {
        _id: 0,
        userid: 1,
        fname: 1,
        email: 1,
        type: 1,
        isActive: 1,
      },
    }
  );

const insertUser = (inputData) => Myconnection.insertOne(inputData);

module.exports = { findUser, findUserByEmail, sharedUser, insertUser };
