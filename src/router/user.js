const Router = require("koa-router");
const { POSTRegister, POSTLogin } = require("../controller/user");
const {
  isEmailExist,
  isUserAlreadyExist,
  errorValidation,
} = require("../validators/user/index");

const routers = new Router({
  prefix: "/user",
});

routers.post("/register", errorValidation, isUserAlreadyExist, POSTRegister);

routers.post("/login", isEmailExist, POSTLogin);

module.exports = routers;
