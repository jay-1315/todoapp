const currentTime = require("../shared/datetime");
const { v4: uuidv4 } = require("uuid");
const { hashedPassword } = require("../shared/password");
const jwt = require("jsonwebtoken");
const { insertUser } = require("../db/query/users");

const POSTRegister = async (ctx) => {
  const { fname, lname, email, password, phone, type } = ctx.request.body;
  const pass = await hashedPassword(password);
  const inputData = {
    userid: uuidv4(),
    fname: fname,
    lname: lname,
    email: email,
    password: pass,
    phone: phone,
    type: type,
    time: currentTime(),
    isActive: false,
  };
  await insertUser(inputData);
  const token = await jwt.sign(
    { id: inputData.userid, email: inputData.email, role: inputData.type },
    process.env.JWT_SECRET
  );
  ctx.body = { message: "Register Success", response: { token } };
};

const POSTLogin = async (ctx) => {
  const { userid, email, type, isActive } = ctx.state.shared;
  if (isActive) {
    const token = await jwt.sign(
      { id: userid, email: email, role: type },
      process.env.JWT_SECRET
    );
    ctx.body = { message: "Login Success", response: { token } };
  } else {
    ctx.status = 401;
    ctx.body = {
      message: "Your Account is not start yet. Please visit youe Admin",
    };
  }
};

module.exports = { POSTRegister, POSTLogin };
