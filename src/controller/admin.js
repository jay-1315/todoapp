const userView = require("../db/query/adminPanel");

const getUserData = async (ctx) => {
  const search = ctx.request.query.search.trim() || "";
  const skip = ctx.request.query.skip || 0;
  const limit = ctx.request.query.limit || 0;
  let searchObj = {};  

  const mydata = await userView(searchObj, skip, limit);
  ctx.body = mydata;
};

module.exports = getUserData;
